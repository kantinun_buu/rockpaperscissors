/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.swingrps;

import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author EAK
 */
public class RPS {

    private int bot, player;//rock: 0, paper: 1, scissors: 2
    private int win, lose, draw;
    private int status;
    private int winstreak;

    public RPS() {
    }

    public int random() {
        return ThreadLocalRandom.current().nextInt(0, 3);
    }

    public int result(int player) { //win: 1, lose: -1, draw: 0
        this.player = player;
        this.bot = random();
        if (this.player == this.bot) {
            return draw();
        } else if (this.player == 0 && this.bot == 2) {
            return win();
        } else if (this.player == 1 && this.bot == 0) {
            return win();
        } else if (this.player == 2 && this.bot == 1) {
            return win();
        } else {
            return lose();
        }
    }

    private int draw() {
        draw++;
        status = 0;
        winstreak = 0;
        return 0;
    }

    private int win() {
        win++;
        status = 1;
        winstreak++;
        return 1;
    }

    private int lose() {
        lose++;
        status = -1;
        winstreak = 0;
        return -1;
    }

    public int getBot() {
        return bot;
    }

    public int getPlayer() {
        return player;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }

    public int getStatus() {
        return status;
    }

    public int getWinstreak() {
        return winstreak;
    }

}
